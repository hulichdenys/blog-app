import Vue from 'vue';
import Router from 'vue-router';
import VueMeta from 'vue-meta';

Vue.use(Router);
Vue.use(VueMeta);

export function createRouter() {
  return new Router({
    mode: 'history',

    routes: [
      {
        path: '/',
        name: 'MainPage',
        component: () =>
          // eslint-disable-next-line
          import(/* webpackChunkName: "main-page" */ './views/Main.vue'),
      },
      {
        path: '/about/',
        name: 'AboutPage',
        component: () =>
          // eslint-disable-next-line
          import(/* webpackChunkName: "about-page" */ './views/About.vue'),
      },
    ],
  });
}
