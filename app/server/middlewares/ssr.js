const path = require('path');

const { createBundleRenderer } = require('vue-server-renderer');

const template = require('fs').readFileSync(
  path.join(__dirname, '../../templates/index.html'),
  'utf-8',
);

const serverBundle = require('../../../dist/vue-ssr-server-bundle.json');
const clientManifest = require('../../../dist/vue-ssr-client-manifest.json');

const renderer = createBundleRenderer(serverBundle, {
  runInNewContext: false,
  template,
  clientManifest,
  inject: false,
});

module.exports = (req, res, next) => {
  const context = { url: req.url };

  renderer.renderToString(context, (err, html) => {
    if (err) {
      if (+err.message === 404) {
        res.status(404).end('Page not found');
      } else {
        // eslint-disable-next-line no-console
        console.log(err);
        res.status(500).end('Internal Server Error');
      }
    }

    res.end(html);
  });

  next();
};
