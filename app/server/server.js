const path = require('path');

const express = require('express');
const { ApolloServer, gql } = require('apollo-server-express');

const ssrMiddleware = require('./middlewares/ssr');

const typeDefs = gql`
  type Query {
    hello: String!
  }
`;

const resolvers = {
  Query: {
    hello() {
      return 'hello world';
    },
  },
};

const app = express();
const server = new ApolloServer({
  typeDefs,
  resolvers,
});

server.applyMiddleware({ app });

app.use('/dist', express.static(path.join(__dirname, '../../dist')));
app.use('/', express.static(path.join(__dirname, '../assets')));

app.use(ssrMiddleware);

app.listen({ port: process.env.PORT || 3000 });
